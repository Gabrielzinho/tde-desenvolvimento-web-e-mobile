﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaWebApi.Models
{
    public class Funcionario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cargo { get; set; }
        public int CPF{ get; set; }
        public bool Ativo { get; set; }
        public List<FuncionarioProjeto> FuncionarioProjeto { get; set; } 
    }
}
